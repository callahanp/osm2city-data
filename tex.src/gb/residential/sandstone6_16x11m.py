
from osm2city.textures.texture import Texture

facades.append(Texture('gb/residential/sandstone6_16x11m.png',
    h_size_meters=16.2, h_cuts=[550, 1128, 1696, 2264, 2784], h_can_repeat=True,
    v_size_meters=11.3, v_cuts=[760, 1535, 2500], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
