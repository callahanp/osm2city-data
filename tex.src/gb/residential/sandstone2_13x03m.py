
from osm2city.textures.texture import Texture

facades.append(Texture('gb/residential/sandstone2_13x03m.png',
    h_size_meters=13.4, h_cuts=[337, 1317, 2326, 3147, 3330, 3660], h_can_repeat=True,
    v_size_meters=2.6, v_cuts=[876], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
