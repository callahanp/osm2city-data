
from osm2city.textures.texture import Texture

facades.append(Texture('gb/residential/sandstone5_06x07m.png',
    h_size_meters=6.4, h_cuts=[701, 1491], h_can_repeat=True,
    v_size_meters=7.4, v_cuts=[725, 1716], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
