
from osm2city.textures.texture import Texture

facades.append(Texture('gb/residential/plaster1_07x03m.png',
    h_size_meters=7.1, h_cuts=[1294, 2438, 3676], h_can_repeat=True,
    v_size_meters=3, v_cuts=[876], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
