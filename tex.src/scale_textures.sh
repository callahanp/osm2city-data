#!/bin/bash
pngs="
DSCF9495_pow2.png:1024
DSCF9495_pow2_LM.png:1024
DSCF9503_noroofsec_pow2.png:1024
DSCF9503_noroofsec_pow2_LM.png:1024
DSCF9678_pow2.png:1024
DSCF9678_pow2_LM.png:1024
DSCF9710.png:1024
DSCF9710_LM.png:1024
DSCF9726_noroofsec_pow2.png:1024
LZ_old_bright_bc2.png:1024
LZ_old_bright_bc2_LM.png:1024
facade_modern36x36_12.png:1024
roof_black2.png:keep
roof_black3.png:keep
roof_tiled_black.png:keep
roof_tiled_red.png:keep
wohnheime_petersburger.png:1024
wohnheime_petersburger_LM.png:1024
"

scale_set ()
{
    local fac=$1
    local out=$2
    mkdir -p $out/
    for png in $pngs; do
	file=`echo $png | cut -d: -f1`
	scale=`echo $png | cut -d: -f2`
	#echo "$file -> $scale"
	if [ "$scale" == "keep" ] || [ "$fac" == "full" ]; then
	    cp $file $out/$file
	else
	    scale=`echo "$scale * $fac" | bc -l | xargs printf "%1.0f"`
	    #echo $scale
	    convert -scale $scale $file $out/$file
	fi
    done
}

scale_set 0.125 tex.tiny
scale_set 0.25  tex.small
scale_set 0.5   tex.medium
scale_set 1.0   tex.high
scale_set full  tex.full
