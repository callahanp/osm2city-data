from osm2city.textures.texture import Texture

facades.append(Texture('us/commercial/50storySteelGlassmodern1.jpg',
    h_size_meters=41, h_cuts=[18, 33, 48, 64], h_can_repeat=True,
    v_size_meters=125, v_cuts=[194], v_can_repeat=False,
    v_align_bottom=False, height_min=41,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('us/commercial/45storyglassmodern.jpg',
    h_size_meters=40, h_cuts=[], h_can_repeat=False,
    v_size_meters=80, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('us/commercial/41storyconcrglasswhitemodern2.jpg',
    h_cuts=[], h_can_repeat=False,
    v_cuts=[], v_can_repeat=False,
    levels=56,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/40storymodern.jpg',
    h_size_meters=35, h_cuts=[21, 42, 52, 64, 74, 85, 96, 106, 118], h_can_repeat=True,
    v_size_meters=64, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=16,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/36storyconcrglassmodern.jpg',
    h_cuts=[], h_can_repeat=False,
    v_cuts=[], v_can_repeat=False,
    levels=36,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/35storyconcrmodernwhite.jpg',
    h_size_meters=25, h_cuts=[], h_can_repeat=False,
    v_size_meters=100, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/30storyconcrbrown4.jpg',
    h_cuts=[], h_can_repeat=False,
    v_size_meters=96, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/27storyConcrBrownGlass.jpg',
    h_cuts=[], h_can_repeat=False,
    v_cuts=[], v_can_repeat=False,
    levels=27,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/25storyBrownWide1.jpg',
    h_cuts=[], h_can_repeat=False,
    v_cuts=[], v_can_repeat=False,
    levels=20,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:gray'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/20storybrownconcrmodern.jpg',
    h_size_meters=44, h_cuts=[20, 42, 63], h_can_repeat=True,
    v_size_meters=56, v_cuts=[30, 35, 40, 46, 51, 57, 62, 68, 74, 80], v_can_repeat=False,
    v_align_bottom=False, height_min=21,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/20storygreycncrglassmodern.jpg',
    h_size_meters=27, h_cuts=[], h_can_repeat=False,
    v_size_meters=54, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/19storyretromodern.jpg',
    h_cuts=[], h_can_repeat=False,
    v_size_meters=100, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('us/commercial/18storyoffice.jpg',
    h_size_meters=28, h_cuts=[], h_can_repeat=False,
    v_size_meters=56, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('us/commercial/15storyltbrownconcroffice3.jpg',
    h_size_meters=29, h_cuts=[], h_can_repeat=False,
    v_size_meters=58, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:gray'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/10storymodernconcrete.jpg',
    h_cuts=[], h_can_repeat=False,
    v_size_meters=40, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/US-dcofficeconcrwhite8st.jpg',
    h_size_meters=30, h_cuts=[], h_can_repeat=True,
    v_size_meters=30, v_cuts=[], v_can_repeat=False,
    v_align_bottom=False, height_min=12,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/US-dchotelDC2_8st.jpg',
    h_size_meters=15, h_cuts=[], h_can_repeat=False,
    v_size_meters=30, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/US-dcofficeconcrwhite6-7st.jpg',
    h_cuts=[], h_can_repeat=False,
    levels=7, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/7storymodernsq.jpg',
    h_size_meters=21, h_cuts=[], h_can_repeat=False,
    v_size_meters=21, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/US-dcdupontconcr5st.jpg',
    h_cuts=[], h_can_repeat=False,
    v_size_meters=15, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=5,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/5storywhite.jpg',
    h_cuts=[], h_can_repeat=False,
    v_size_meters=15, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=5,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/US-dcgovtconcrtan4st.jpg',
    h_cuts=[], h_can_repeat=False,
    levels=5, v_cuts=[], v_can_repeat=False,
    v_align_bottom=False, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

# here

facades.append(Texture('us/commercial/3storystorefronttown.jpg',
    h_size_meters=9, h_cuts=[], h_can_repeat=False,
    v_size_meters=9, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/salmon_3_story_0_scale.jpg',
    h_size_meters=8, h_cuts=[], h_can_repeat=False,
    v_size_meters=8, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/2stFancyconcrete1.jpg',
    h_size_meters=14, h_cuts=[], h_can_repeat=False,
    v_size_meters=7, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:gray'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/US-dcwhiteconcr2st.jpg',
    h_size_meters=16, h_cuts=[], h_can_repeat=False,
    v_size_meters=8, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/US-dctbrickcomm2st.jpg',
    h_size_meters=21, h_cuts=[], h_can_repeat=False,
    v_size_meters=5.25, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:gray'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/USUAE-4stCommercial.jpg',
    h_size_meters=20, h_cuts=[], h_can_repeat=False,
    v_size_meters=10, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/US-OfficeComm-2st.jpg',
    h_size_meters=15, h_cuts=[], h_can_repeat=False,
    v_size_meters=3.75, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/US-1stCommWarehousewhite1.jpg',
    h_size_meters=15, h_cuts=[], h_can_repeat=False,
    v_size_meters=3.75, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/US-1stCommBrick2.jpg',
    h_size_meters=15, h_cuts=[], h_can_repeat=False,
    v_size_meters=3.75, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


facades.append(Texture('us/commercial/US-1stCommStFront3.jpg',
    h_size_meters=10, h_cuts=[], h_can_repeat=False,
    v_size_meters=5, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('us/residential/tiles/USUAE-8stTile_rep.jpg',
    h_size_meters=15, h_cuts=[], h_can_repeat=False,
    v_size_meters=30, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('us/residential/6storybrickbrown1.jpg',
    h_size_meters=21, h_cuts=[], h_can_repeat=False,
    v_size_meters=21, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('us/residential/5storyCondo_concrglasswhite.jpg',
    h_size_meters=14, h_cuts=[], h_can_repeat=False,
    v_size_meters=28, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('us/residential/US-CityCondo_brick_4st.jpg',
    h_size_meters=16, h_cuts=[], h_can_repeat=False,
    v_size_meters=16, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('us/residential/US-CityCondo2st.jpg',
    h_size_meters=11, h_cuts=[], h_can_repeat=False,
    v_size_meters=11, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True,
    requires=['roof:colour:grey'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('us/commercial/US-OfficeModern-42st.jpg',
    h_size_meters=48, h_cuts=[18, 38, 57, 76, 94, 112, 128], h_can_repeat=True,
    v_size_meters=144, v_cuts=[], v_can_repeat=False,
    v_align_bottom=False, height_min=36,
    requires=['roof:colour:gray'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))
