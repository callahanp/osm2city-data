# -*- coding: utf-8 -*-
from osm2city.textures.texture import Texture


roofs.append(Texture('roof_red1.png',
                     32, [0, 1024], True, 16, [0, 512], False,
                     provides=['default', 'colour:red',
                               'compat:roof-pitched', 'compat:roof-flat',
                               'material:roof_tiles']))

roofs.append(Texture('roof_red2.png',
                     32, [0, 1024], True, 16, [0, 512], False,
                     provides=['default', 'colour:red',
                               'compat:roof-pitched',
                               'material:roof_tiles']))

roofs.append(Texture('roof_red3.png',
                     32, [0, 1024], True, 16, [0, 512], False,
                     provides=['default', 'colour:red',
                               'compat:roof-pitched',
                               'material:roof_tiles']))

roofs.append(Texture('roof_orange.png',
                     32, [0, 1024], True, 16, [0, 512], False,
                     provides=['specific', 'colour:orange',
                               'compat:roof-pitched',
                               'material:roof_tiles']))

roofs.append(Texture('roof_black1.png',
                     32, [0, 2048], True, 16, [0, 1024], False,
                     provides=['default', 'colour:black',
                               'compat:roof-pitched',
                               'material:roof_tiles']))

roofs.append(Texture('roof_black_slate.png',
                     32, [0, 2048], True, 16, [0, 1024], False,
                     provides=['colour:black',
                               'compat:roof-pitched',
                               'material:slate']))

roofs.append(Texture('roof_stone.png',
                     100., [], True, 100., [], False,
                     requires=['roof:material:stone'],
                     provides=['specific', 'colour:white', 'colour:yellow',
                               'compat:roof-flat', 'compat:roof-pitched',
                               'material:stone']))

roofs.append(Texture('roof_stone_red.png',
                     100., [], True, 100., [], False,
                     requires=['roof:material:stone'],
                     provides=['specific', 'colour:red',
                               'compat:roof-flat', 'compat:roof-pitched',
                               'material:stone']))

roofs.append(Texture('roof_gen_black1.png',
                     100., [], True, 100., [], False,
                     provides=['default', 'colour:black',
                               'compat:roof-flat', 'compat:roof-large']))

roofs.append(Texture('roof_gen_darkgrey.png',
                     100., [], True, 100., [], False,
                     provides=['default', 'colour:darkgrey',
                               'compat:roof-flat', 'compat:roof-pitched', 'compat:roof-large']))

roofs.append(Texture('roof_gen_grey.png',
                     100., [], True, 100., [], False,
                     provides=['default', 'colour:grey',
                               'compat:roof-flat', 'compat:roof-pitched', 'compat:roof-large']))

roofs.append(Texture('roof_gen_grey_large.png',
                     100., [], True, 400., [], False,
                     provides=['default', 'compat:roof-large']))

roofs.append(Texture('roof_gen_lightgrey.png',
                     100., [], True, 100., [], False,
                     provides=['default', 'colour:lightgrey',
                               'compat:roof-flat', 'compat:roof-pitched', 'compat:roof-large']))

roofs.append(Texture('roof_gen_metal.png',
                     100., [], True, 100., [], False,
                     provides=['default', 'colour:metal', 'color:grey'
                               'compat:roof-flat',
                               'material:metal']))

roofs.append(Texture('roof_gen_brown1.png',
                     100., [], True, 100., [], False,
                     provides=['default', 'colour:brown',
                               'compat:roof-flat']))

roofs.append(Texture('roof_gen_grass.png',
                     100., [], True, 100., [], False,
                     requires=['roof:material:grass'],
                     provides=['specific', 'colour:green',
                               'compat:roof-pitched', 'compat:roof-flat',
                               'material:grass']))

roofs.append(Texture('roof_gen_copper.png',
                     100., [], True, 100., [], False,
                     requires=['roof:material:copper'],
                     provides=['specific', 'colour:green',
                               'compat:roof-pitched', 'compat:roof-flat',
                               'material:copper']))

roofs.append(Texture('roof_gen_glass.png',
                     100., [], True, 100., [], False,
                     requires=['roof:material:glass'],
                     provides=['specific',
                               'compat:roof-pitched', 'compat:roof-flat',
                               'material:glass']))

